let collection = [];



//  show all the items in the stack -collection
// 1-returns: []
function print(){
    return collection;
};

// push an element in the stack - enqueue
// 2-returns: John
// 3-returns: John , Jane
function enqueue(el){
	let arr =[el];
	collection = collection.concat(arr);
	return collection
}

//  pop an element in the stack - dequeue
// 4-returns: Jane

function dequeue(){
	collection.splice(0,1);
	return collection;
}

//  show the top element in the stack -front
// 5- returns: Jane, Bob
// 6- returns: Jane, Bob, Cherry
function front(){
	return collection[0];
}
// return the size of the stack - 
// 6- returns: 3
function size(){
	return collection.length;
}
// return if there are items in the stack or not - 
// 6- returns: false
function isEmpty(){
	let arr = collection.length;
	if (arr ===0){return true;}
	else{return false} 
	
}









module.exports = {
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty

};
